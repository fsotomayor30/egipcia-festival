import 'package:egipcia_festival/Modulos/Staff/Staff.dart';
import 'package:egipcia_festival/Modulos/Video/ReproductorVideo.dart';
import 'package:flutter/material.dart';
import 'package:egipcia_festival/Modulos/Auspiciadores/Auspiciadores.dart';
import 'package:egipcia_festival/Modulos/Galeria/Fotos.dart';

import 'package:egipcia_festival/Modulos/Inicio/Inicio.dart';
import 'package:egipcia_festival/Modulos/Video/VideoPage.dart';

import 'package:url_launcher/url_launcher.dart';

class MenusPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MenuList(),
    );
  }
}

class MenuList extends StatelessWidget {
  List<Widget> containers = [
    Container(child: InicioPage()),
    Container(child: FotosPage(),),
    Container(child: VideoPage(),),
    Container(child: Auspiciadores(),),
  ];

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;

    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(0, 0, 0, 1),
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                child: Text("EGIPCIA FESTIVAL", style: TextStyle(
                  fontFamily: 'Avenir',
                  fontSize: mediaQueryData.width*0.07,
                  color: Colors.white,
                )),
              ),
              // Text('iChillán'),
            ],
          ),
          bottom: TabBar(indicatorColor: Colors.white, tabs: <Widget>[
            Tab(icon: Icon(Icons.home),),
            Tab(icon: Icon(Icons.image)),
            Tab(icon: Icon(Icons.play_circle_filled)),
            Tab(icon: Icon(Icons.group))
          ]),
        ),
        body: TabBarView(children: containers),
        drawer: Drawer(
          // Add a ListView to the drawer. This ensures the user can scroll
          // through the options in the drawer if there isn't enough vertical
          // space to fit everything.
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/Imagenes/fondo app.jpg"), fit: BoxFit.cover)            ),
            child: ListView(
              // Important: Remove any padding from the ListView.
              padding: EdgeInsets.zero,
              children: <Widget>[
                DrawerHeader(
                    child: Image.asset('assets/Imagenes/egipcialogogrande.png'),
                    decoration: BoxDecoration(
                      color: Color.fromRGBO(0,0,0,1),
/*                      image: DecorationImage(
                        image: NetworkImage(
                          'https://firebasestorage.googleapis.com/v0/b/egipciafestival-45700.appspot.com/o/piramides.jpg?alt=media&token=27ff8a9d-bfcb-4de9-acb6-461c62e70e08',
                        ),
                        fit: BoxFit.cover,

                      ),*/
                    )
                    //color: Color.fromRGBO(238,153,61,1),
                    ),
                ListTile(
                  title: Text(
                    'Facebook',
                    style: TextStyle(fontFamily: 'Avenir', color: Colors.white),
                  ),
                  leading: CircleAvatar(
                    child: Image.asset('assets/Imagenes/facebook.png'),
                    backgroundColor: Colors.transparent,
                  ),
                  onTap: () async {
                    if (await canLaunch(
                        "https://www.facebook.com/Egipcia-Festival-108757777339739/")) {
                      await launch("https://www.facebook.com/Egipcia-Festival-108757777339739/");
                    }
                  },
                  trailing: Icon(
                    Icons.keyboard_arrow_right,
                    color: Colors.white,
                  ),
                ),
                ListTile(
                  title: Text(
                    'Instagram',
                    style: TextStyle(fontFamily: 'Avenir', color: Colors.white),
                  ),
                  leading: CircleAvatar(
                    child: Image.asset('assets/Imagenes/instagram.png'),
                    backgroundColor: Colors.transparent,
                  ),
                  trailing: Icon(
                    Icons.keyboard_arrow_right,
                    color: Colors.white,
                  ),
                  onTap: () async {
                    if (await canLaunch(
                        "https://www.instagram.com/egipciafestival/")) {
                      await launch(
                          "https://www.instagram.com/egipciafestival/");
                    }
                  },
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }
}
