class Option {
  final String title;
  final String imagen;

  const Option({this.title, this.imagen});
}

const ListTiempoLibre = const<Option> [
  const Option(
    title: 'Deporte',
    imagen : 'assets/Imagenes/TiempoLibre/canchas.png',
  ),
  const Option(
      title: 'Entretención',
      imagen: 'assets/Imagenes/TiempoLibre/entretencion.png',
  ),
  const Option(
      title: 'Ruta Cultural',
      imagen: 'assets/Imagenes/TiempoLibre/rutacultural.png',
  ),
  const Option(
      title: 'Plazas',
      imagen: 'assets/Imagenes/TiempoLibre/plazas.png',
  ),
  const Option(
      title: 'Hotelería',
      imagen: 'assets/Imagenes/TiempoLibre/hoteleria.png',
  ),
  const Option(
      title: 'Restaurantes',
      imagen: 'assets/Imagenes/TiempoLibre/restorant.png',
  ),
  const Option(
      title: 'Ciclovías',
      imagen: 'assets/Imagenes/TiempoLibre/ciclovia.png',
  ),
];

const ListServicios = const<Option> [
  const Option(
      title: 'Veterinarias',
      imagen: 'assets/Imagenes/Servicios/veterinaria.png',
  ),
  const Option(
      title: 'Educación',
      imagen: 'assets/Imagenes/Servicios/educacion.png',
  ),
  const Option(
      title: 'Farmacias',
      imagen: 'assets/Imagenes/Servicios/farmacia.png',
  ),
  const Option(
      title: 'Servicios Públicos',
      imagen: 'assets/Imagenes/Servicios/edificioPublico.png',
  ),
  const Option(
      title: 'Estación de Bencina',
      imagen: 'assets/Imagenes/Servicios/gasolina.png',
  ),
  const Option(
      title: 'Emergencias',
      imagen: 'assets/Imagenes/Servicios/emergencias.png',
  ),
  const Option(
      title: 'Guía Ocupacional',
      imagen: 'assets/Imagenes/Servicios/guiaOcupacional.png',
  ),
  const Option(
    title: 'Supermercados',
    imagen: 'assets/Imagenes/Servicios/supermercado.png',
  )
];