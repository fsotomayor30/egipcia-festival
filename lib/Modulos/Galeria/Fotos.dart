import 'package:flutter/material.dart';

import 'package:url_launcher/url_launcher.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:transparent_image/transparent_image.dart';


class FotosPageState extends State<FotosPage> {


  @override
  void initState() {
    super.initState();

  }

  @override
  void dispose() {
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;
    Widget w_fotos;
    Widget w_djs;


    w_fotos=
        StreamBuilder(
            stream: Firestore.instance.collection("NuestrosEventos").snapshots(),
            builder: (context, snapshot){
              if(!snapshot.hasData) return Container();
              return
                InkWell(
                      splashColor: Colors.blue.withAlpha(30),
                      child: Container(
                        decoration: new BoxDecoration(
                        ),
                        //width: mediaQueryData.width*0.9,
                        //height: mediaQueryData.height*0.30,
                        child:Column(
                          children: <Widget>[
                            CarouselSlider(
                              //height: mediaQueryData.height*0.5,
                              autoPlayInterval: Duration(seconds: 5),
                              autoPlayAnimationDuration: Duration(milliseconds: 800),
                              autoPlay: true,
                              items: snapshot.data.documents.map<Widget>((DocumentSnapshot document) {
                                print(document['url']);
                                return Builder(
                                  builder: (BuildContext context) {
                                    return Container(
                                      width: MediaQuery.of(context).size.width,
                                      height: 2,
                                      margin: EdgeInsets.symmetric(horizontal: 5.0),
                                      //decoration: BoxDecoration(
                                      //    color: Colors.amber
                                      //),
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
                                        child:
                                        Stack(
                                          children: <Widget>[
                                            Center(child: Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: CircularProgressIndicator(),
                                            )),
                                            Center(
                                              child:FadeInImage.memoryNetwork(
                                                placeholder: kTransparentImage,
                                                image: document['url'],
                                              ),
                                            ),
                                          ],
                                        ),


                                      ),
                                    );
                                  },
                                );
                              }).toList(),
                            ),
                          ],
                        ),
                      ),
                    );
            });
    w_djs=
        StreamBuilder(
            stream: Firestore.instance.collection("Djs").snapshots(),
            builder: (context, snapshot){
              if(!snapshot.hasData) return Container();
              return
                InkWell(
                      splashColor: Colors.blue.withAlpha(30),
                      child: Container(
                        decoration: new BoxDecoration(
                        ),
                        child:Column(
                          children: <Widget>[
                            CarouselSlider(
                              height: mediaQueryData.height*0.5,
                              autoPlayInterval: Duration(seconds: 5),
                              autoPlayAnimationDuration: Duration(milliseconds: 800),
                              autoPlay: true,
                              items: snapshot.data.documents.map<Widget>((DocumentSnapshot document) {
                                return Builder(
                                  builder: (BuildContext context) {
                                    return Container(
                                      //width: mediaQueryData.width,
                                      //height: mediaQueryData.height*2,
                                      margin: EdgeInsets.symmetric(horizontal: 5.0),
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
                                        child:
                                        Stack(
                                          children: <Widget>[
                                            Center(child: Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: CircularProgressIndicator(),
                                            )),
                                            Center(
                                              child:FadeInImage.memoryNetwork(
                                                placeholder: kTransparentImage,
                                                image: document['url'],
                                              ),
                                            ),
                                          ],
                                        ),



                                      ),
                                    );
                                  },
                                );
                              }).toList(),
                            ),
                          ],
                        ),
                      ),
                );
            });



    return Scaffold(
        body: //Column(
        //children: <Widget>[
        Container(
          height: mediaQueryData.height,
          decoration: new BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/Imagenes/fondo app.jpg"), fit: BoxFit.cover)          ),
          child: (SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Center(
                    child: w_fotos,
                  ),

              Padding(
                padding:  EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Text("Line Up",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontFamily: 'Avenir',
                        color: Colors.white,
                        fontSize: 25
                    )
                ),
              ),
                  Center(
                    child: w_djs
                  )

                ],
              ))


              //],
              //)
          ),
        )
    );
  }
}

class FotosPage extends StatefulWidget {
  @override
  createState() => FotosPageState();
}
