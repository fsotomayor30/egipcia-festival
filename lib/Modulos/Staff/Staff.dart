import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:egipcia_festival/Modulos/General/Loading.dart';

class StaffScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new StaffList();
}

class StaffList extends State<StaffScreen> {
  @override
  initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget buildListItem(BuildContext context, DocumentSnapshot document) {
    return new ListTile(
      title: (Text(
        document['Nombre'],
        style: TextStyle(color: Colors.black),
      )),
      leading: Image.asset("assets/Imagenes/momia.png"),
      trailing: Icon(
        Icons.keyboard_arrow_right,
        color: Colors.black,
      ),
      onTap: () async {
        if (await canLaunch(document['Instagram'])) {
          await launch(document['Instagram']);
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(192, 121, 4, 1),
          title: Text("Nuestro Staff"),
          centerTitle: true,
          bottom: PreferredSize(
              child: Text("Presiona un staff",
                  style: TextStyle(color: Colors.white, fontSize: 14.0)),
              preferredSize: null),
        ),
        body: Container(
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: StreamBuilder(
                stream: Firestore.instance.collection("Staff").snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) return Loading();
                  return Column(
                    children: <Widget>[
                      Expanded(
                        child: ListView.builder(
                          //itemExtent: 80.0,
                          itemCount: snapshot.data.documents.length,
                          itemBuilder: (context, index) => buildListItem(
                              context, snapshot.data.documents[index]),
                        ),
                      ),
                    ],
                  );
                })));
  }
}
