import 'package:flutter/material.dart';
import 'package:egipcia_festival/Modulos/Inicio/Configuracion.dart';

import 'package:url_launcher/url_launcher.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:transparent_image/transparent_image.dart';

class InicioPageState extends State<InicioPage> {
  List<Configuracion> configurationList = [];
  String bannerPrincipal = "";

  @override
  void initState() {
    super.initState();

    CollectionReference configurationRef =
        Firestore.instance.collection("Configuracion");

    configurationRef.snapshots().listen((querySnapshot) {
      querySnapshot.documentChanges.forEach((document) {
        if (document.document['activado'] == "Verdadero") {
          Configuracion configuration = new Configuracion(
              document.document['activado'],
              document.document['texto'],
              document.document['url'],
              document.document['color1'],
              document.document['color2'],
              document.document['color3']);
          //print(document.document['url']);
          configurationList.add(configuration);
        }
      });
      setState(() {
        print('Length: ' + configurationList.length.toString());
      });
    });

    CollectionReference configurationRefBanner =
        Firestore.instance.collection("BannerPrincipal");

    configurationRefBanner.snapshots().listen((querySnapshot) {
      querySnapshot.documentChanges.forEach((document) {
        bannerPrincipal = document.document['url'];
      });
      setState(() {
        print('Banner: ' + bannerPrincipal);
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;
    Widget w_eventos;
    Widget w_portada = (bannerPrincipal == "")
        ? Container()
        : Stack(
            children: <Widget>[

              Center(
                child: FadeInImage.memoryNetwork(
                  placeholder: kTransparentImage,
                  image: bannerPrincipal,
                ),
              ),
            ],
          );

    Widget w_inicio = Text("¿Te lo vas a perder?",
        textAlign: TextAlign.center,
        style:
            TextStyle(color: Colors.white, fontFamily: 'Avenir', fontSize: 25));

    w_eventos = StreamBuilder(
        stream: Firestore.instance.collection("Fotos").snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return Container();
          return Card(
              child: InkWell(
            splashColor: Colors.blue.withAlpha(30),
            child: Container(
              //height: mediaQueryData.height*0.30,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                    child: Text('Próximos eventos',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Avenir',
                            fontSize: 15)),
                  ),
                  CarouselSlider(
                    //height: 400.0,
                    autoPlayInterval: Duration(seconds: 5),
                    autoPlayAnimationDuration: Duration(milliseconds: 800),
                    autoPlay: true,
                    items: snapshot.data.documents
                        .map<Widget>((DocumentSnapshot document) {
                      return Builder(
                        builder: (BuildContext context) {
                          return Container(
                            width: MediaQuery.of(context).size.width,
                            height: 2,
                            margin: EdgeInsets.symmetric(horizontal: 5.0),
                            //decoration: BoxDecoration(
                            //    color: Colors.amber
                            //),
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
                                child: Image.network(document['url'])),
                          );
                        },
                      );
                    }).toList(),
                  ),
                ],
              ),
            ),
          ));
        });

    return Scaffold(
        body: Container(
      height: mediaQueryData.height,
      decoration: new BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/Imagenes/fondo app.jpg"), fit: BoxFit.cover)
      ),

      child: (SingleChildScrollView(
          child: Column(
            children: <Widget>[
          Center(
            child: w_portada,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Center(
              child: w_inicio,
            ),
          ),
          configurationList.length == 0
              ? new Container()
              : ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: configurationList.length,
                  itemBuilder: (_, index) => configurationUI(
                      configurationList[index].activado,
                      configurationList[index].texto,
                      configurationList[index].url,
                      configurationList[index].color1,
                      configurationList[index].color2,
                      configurationList[index].color3))
        ],
      ))),
    ));
  }

  Widget configurationUI(String activado, String texto, String url,
      String color1, String color2, String color3) {
    final mediaQueryData = MediaQuery.of(context).size;

    return GestureDetector(
      onTap: () async {
        if (await canLaunch(url)) {
          await launch(url);
        }
      },
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Card(
            child: InkWell(
                splashColor: Colors.blue.withAlpha(30),
                child: Container(
                  width: mediaQueryData.width - 20,
                  height: 70,
                  decoration: new BoxDecoration(
                      color: Color.fromRGBO(int.parse(color1), int.parse(color2),
                          int.parse(color3), 1)),
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Stack(
                      children: <Widget>[
                        (texto == "Facebook") ? Container(alignment: Alignment.centerLeft,
                      child: Image.asset("assets/Imagenes/facebook.png")) : (texto == "Instagram") ? Container(alignment: Alignment.centerLeft,
                            child: Image.asset("assets/Imagenes/instagram.png")) : (texto == "¿DÓNDE ES EL FESTIVAL?") ? Container(
                        alignment: Alignment.centerLeft,
                        child: Image.asset("assets/Imagenes/mapa.png")): Container(
                          alignment: Alignment.centerLeft,
                          child: Image.asset("assets/Imagenes/momia.png"),
                        ),
                        Center(
                          child: Text(texto,
                              //textAlign: TextAlign.left,
                              style: TextStyle(
                                fontFamily: 'Avenir',
                                color: Colors.white,
                              )),
                        ),

                      ],
                    ),
                  ),
                ))),
      ),
    );
  }
}

class InicioPage extends StatefulWidget {
  @override
  createState() => InicioPageState();
}
